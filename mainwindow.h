#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ui_mainwindow.h"
#include "interface_wid/iw_start_form.h"
#include "interface_wid/iw_patch_main.h"
#include "interface_wid/iw_patchlighting.h"
#include "datamodel/remote_register.h"
#include "datamodel/register_manager.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE
#include "interface/interface_controller.h"
/*
typedef struct
{
    QString InterfaceName;
    QWidget* InterfaceWidget;
    QString Parent_InterfaceName; // Родительский
}*/

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


public slots:
    void SetActiveWidgetByNmae(QString);
    void SetActiveWidget(quint8);

private slots:
    void on_pushB_clicked();
    void on_cw_clicked();

    void on_ccw_clicked();

    void on_pushButton_clicked();

    void on_sb_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

private:
    Ui::MainWindow *ui;
   // IW_start_form IW_start;
   // IW_patch_main IW_Patch_A;
   // IW_PatchLighting IW_Patch_A_Lightting;
   // RegisterManager RegMan; // Менеджер и хранилище данных регистров
    //QList<RemoteRegister*> Registers; // Основной массив регстров устройства

};
#endif // MAINWINDOW_H
