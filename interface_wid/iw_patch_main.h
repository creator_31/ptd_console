#ifndef IW_PATH_MAIN_H
#define IW_PATH_MAIN_H

#include <QWidget>

namespace Ui {
class IW_patch_main;
}

class IW_patch_main : public QWidget
{
    Q_OBJECT

public:
    explicit IW_patch_main(QWidget *parent = nullptr);
    ~IW_patch_main();
    void SetInterfaceName(QString NewName); // Установка названия экрана
    void SetPatch(quint8 NewPatchNumber, QString NewPatchCode);// Установка номера грядки
signals:
    void NavigateToInterface(QString );

private slots:
    void on_pushButton_clicked();

private:
    Ui::IW_patch_main *ui;
    quint8 PatchNumber; // Номер грядки (0-A, 1-B, 2-C, 3-D, 4-F...) используется для настройки связи с параметрами ( по коду)
    QString PatchCode; // Код грядки ( для навигации)
};

#endif // IW_PATH_MAIN_H
