#include "iw_patchlighting.h"
#include "ui_iw_patchlighting.h"

IW_PatchLighting::IW_PatchLighting(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::IW_PatchLighting)
{
    ui->setupUi(this);
}

IW_PatchLighting::~IW_PatchLighting()
{
    delete ui;
}

void IW_PatchLighting::InitPatchInterface(QString InterfaceCode)
{

}

void IW_PatchLighting::SetRegisterManager(RegisterManager *RegisterManagerLink)
{
    this->RegisterManagerLink = RegisterManagerLink;
    ui->start_day->LinkRegister(RegisterManagerLink->GetRegisterByCode(30204 + PatchNumber*1000 ));
    ui->day_long->LinkRegister(RegisterManagerLink->GetRegisterByCode(30205 + PatchNumber*1000));
}

void IW_PatchLighting::SetPatchNumber(quint8 NewPatchNumber)
{
    this->PatchNumber = NewPatchNumber;
}
