#include "iw_patch_main.h"
#include "ui_iw_patch_main.h"

IW_patch_main::IW_patch_main(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::IW_patch_main)
{
    ui->setupUi(this);
}

IW_patch_main::~IW_patch_main()
{
    delete ui;
}

void IW_patch_main::SetInterfaceName(QString NewName)
{
    ui->InterfaceName->setText(NewName);
}

void IW_patch_main::SetPatch(quint8 NewPatchNumber, QString NewPatchCode)
{
    this->PatchNumber = NewPatchNumber;
    this->PatchCode = NewPatchCode;
    QString PatchCode;
    switch (NewPatchNumber) {
        case 0: PatchCode = "A"; break;
        case 1: PatchCode = "B"; break;
        case 2: PatchCode = "C"; break;
        case 3: PatchCode = "D"; break;
        case 4: PatchCode = "E"; break;
        case 5: PatchCode = "F"; break;
    default: PatchCode = "?"; break;
    }
    ui->InterfaceName->setText("Грядка " + PatchCode);
}

void IW_patch_main::on_pushButton_clicked()
{
    emit NavigateToInterface(PatchCode + "-LIGHTING");
    // emit this->NavigateToInterface("PATCH_A-LIGHTING");
}
