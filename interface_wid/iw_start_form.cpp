#include "iw_start_form.h"
#include "ui_iw_start_form.h"
#include <QtWidgets/QStackedWidget>

IW_start_form::IW_start_form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::IW_start_form)
{
    ui->setupUi(this);
   // connect(this,SIGNAL(GoTo_IW_Patch(quit8)),parentWidget()->parentWidget()->parentWidget(), SLOT(SetActiveWidget(quint8)));

}

IW_start_form::~IW_start_form()
{
    delete ui;
}

void IW_start_form::on_nav_Patch_A_clicked()
{
    QStackedWidget *stack = qobject_cast<QStackedWidget* >(parentWidget());
        if(stack)
            stack->setCurrentIndex(1);
}
