#include "itw_timeedit.h"
#include "ui_itw_timeedit.h"

ITW_TimeEdit::ITW_TimeEdit(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ITW_TimeEdit)
{
    ui->setupUi(this);
}

ITW_TimeEdit::~ITW_TimeEdit()
{
    delete ui;
}

void ITW_TimeEdit::SetRegister(quint16 RegisterCode)
{

}

void ITW_TimeEdit::LinkRegister(RemoteRegister *RemoteRegister)
{
    RegisterObj = RemoteRegister;
    ui->param_name->setText(RegisterObj->RegisterName);
    connect(RemoteRegister,SIGNAL(RegisterUpdated()),this,SLOT(RegisterResync()));
    RegisterResync(); // Первонаяальная отрисовка
}

void ITW_TimeEdit::RegisterResync()
{
    if(RegisterObj->SyncStatus == REG_SYNCSTATUS_SYNCRONIZED) // Если регистр актуальный
    {
        ui->param_value->setStyleSheet("#param_value{\n	\n	background-color: rgb(211, 215, 207);\n	\n	color: rgb(21, 73, 16);\nfont-size:20px}\n\n#param_value:checked{\n	background-color: rgb(0, 151, 255);\ncolor:white;\nfont-size:20px}");
    }
    if(RegisterObj->SyncStatus == REG_SYNCSTATUS_NEED_READ) // Если регистр в очереди на чтение
    {
        ui->param_value->setStyleSheet("#param_value{\n	\n	background-color: rgb(211, 215, 207);\n	\n	color: rgb(21, 73, 16);\nfont-size:20px}\n\n#param_value:checked{\n	background-color: rgb(0, 151, 255);\ncolor:white;\nfont-size:20px}");
    }
    else if(RegisterObj->SyncStatus == REG_SYNCSTATUS_NEED_WRITE) // Если регистр обновился из консоли
    {
        ui->param_value->setStyleSheet("#param_value{\n	background-color: rgb(245, 121, 0);\n	color: rgb(238, 238, 236);\nfont-size:20px}\n\n#param_value:checked{\n	background-color: rgb(245, 121, 0);\n	color: rgb(238, 238, 236);\nfont-size:20px}");
        ui->param_value->setCheckable(false);
    }
    else if(RegisterObj->SyncStatus == REG_SYNCSTATUS_TIMEOUT) // Если тайм-аут обновления
    {
        ui->param_value->setStyleSheet("#param_value{\n	\n	background-color: rgb(204, 0, 0);\n	color: rgb(238, 238, 236);\nfont-size:20px}\n\n#param_value:checked{\n	background-color: rgb(204, 0, 0);\n	color: rgb(238, 238, 236);\nfont-size:20px}");
    }
    else if(RegisterObj->SyncStatus == REG_SYNCSTATUS_UNDEF) // Если регистр только создан
    {
        ui->param_value->setStyleSheet("#param_value{\n	background-color: rgb(46, 52, 54);\n	color: rgb(136, 138, 133);\nfont-size:20px}\n\n#param_value:checked{\n	background-color: rgb(46, 52, 54);\n	color: rgb(136, 138, 133);\nfont-size:20px}");
    }

    if((RegisterObj->ReadWriteType == REG_READWRITETYPE_READWRITE) // Если регистр может быть записан
        && (RegisterObj->SyncStatus == REG_SYNCSTATUS_SYNCRONIZED || RegisterObj->SyncStatus == REG_SYNCSTATUS_NEED_READ))
    {
        ui->param_value->setCheckable(true);
    }
    else
    {
        ui->param_value->setChecked(false);
        ui->param_value->setCheckable(false);
    }

    if(RegisterObj->Value() < 96) // Время корректное
    {
        quint8 hrs;
        quint8 min;
        QString res_val;
        hrs = RegisterObj->Value() / 4; // Вычисляю кол-во часов
        min = (RegisterObj->Value() - hrs*4)*15; // Вычисление минут
        res_val.sprintf("%02u:%02u", hrs,min);
        ui->param_value->setText(res_val);
    }
    else
    {
        ui->param_value->setText("--:--");
    }

    //ui->param_value->setText(QString::number(RegisterObj->Value()));
}
