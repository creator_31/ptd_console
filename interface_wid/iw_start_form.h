#ifndef IW_START_FORM_H
#define IW_START_FORM_H

#include <QWidget>

namespace Ui {
class IW_start_form;
}

class IW_start_form : public QWidget
{
    Q_OBJECT

public:
    explicit IW_start_form(QWidget *parent = nullptr);
    ~IW_start_form();

private slots:
    void on_nav_Patch_A_clicked();

private:
    Ui::IW_start_form *ui;
};

#endif // IW_START_FORM_H
