#ifndef IW_PATCHLIGHTING_H
#define IW_PATCHLIGHTING_H

#include <QWidget>
#include "datamodel/register_manager.h"

namespace Ui {
class IW_PatchLighting;
}

class IW_PatchLighting : public QWidget
{
    Q_OBJECT

public:
    explicit IW_PatchLighting(QWidget *parent = nullptr);
    ~IW_PatchLighting();
    void InitPatchInterface(QString InterfaceCode);
    void SetRegisterManager(RegisterManager* RegisterManagerLink);
    void SetPatchNumber(quint8 NewPatchNumber);// Установка номера грядки
signals:
    void NavigateToInterFace(QString);

private:
    Ui::IW_PatchLighting *ui;
    RegisterManager* RegisterManagerLink;
    quint8 PatchNumber; // Номер грядки (0-A, 1-B, 2-C, 3-D, 4-F...) используется для настройки связи с параметрами ( по коду)
};

#endif // IW_PATCHLIGHTING_H
