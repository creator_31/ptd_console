#ifndef ITW_TIMEEDIT_H
#define ITW_TIMEEDIT_H

#include <QWidget>
#include "datamodel/register_manager.h"


namespace Ui {
class ITW_TimeEdit;
}

class ITW_TimeEdit : public QWidget
{
    Q_OBJECT

public:
    explicit ITW_TimeEdit(QWidget *parent = nullptr);
    ~ITW_TimeEdit();
    void SetRegister(quint16 RegisterCode); // Привязка элемента управления к регистру
    void LinkRegister(RemoteRegister* RemoteRegister);

//signals:
//    void Focus();
public slots:
    void RegisterResync(); // Слот обновления данных в регистре
  //  void FocusLost(); // Потеря фокуса
  //  void IncreseValue(); // Увеличить значение
  //  void DecreseValue(); // Уменьшить значение
  //  void OKButton(); // Нажали на кнопку энкодера
  //  void CancelButton(); // Нажали на кнопку отмены/назад
private:
    Ui::ITW_TimeEdit *ui;
    quint16 RegisterCode;
    RemoteRegister* RegisterObj;
    quint8 DisplayValue; // Значение, отображаемое на форме
    bool EditMode; // Флаг нахождения в режимер редактирования
};

#endif // ITW_TIMEEDIT_H
