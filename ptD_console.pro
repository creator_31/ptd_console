QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
#CONFIG += console

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    datamodel/register_manager.cpp \
    datamodel/remote_register.cpp \
    interface/interface_controller.cpp \
    interface_wid/itw_timeedit.cpp \
    interface_wid/iw_patch_main.cpp \
    interface_wid/iw_patchlighting.cpp \
    interface_wid/iw_start_form.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    datamodel/register_manager.h \
    datamodel/remote_register.h \
    interface/interface_controller.h \
    interface_wid/itw_timeedit.h \
    interface_wid/iw_patch_main.h \
    interface_wid/iw_patchlighting.h \
    interface_wid/iw_start_form.h \
    mainwindow.h

FORMS += \
    interface/interfacecontroller.ui \
    interface_wid/itw_timeedit.ui \
    interface_wid/iw_patch_main.ui \
    interface_wid/iw_patchlighting.ui \
    interface_wid/iw_start_form.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
