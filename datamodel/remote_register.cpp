#include "remote_register.h"

RemoteRegister::RemoteRegister(QObject *parent) : QObject(parent)
{
 NewValue = 0;
}

quint8 RemoteRegister::Value()
{
    return CurrentValue;

}

void RemoteRegister::setValue(quint8 newValue)
{
    NewValue=newValue;
    // !!!!!!!!!!!!!!! Заглушка временная
    CurrentValue = newValue; // !!!!!!!!!!!!!!!!!!!!!!
    emit RegisterUpdated();
}
