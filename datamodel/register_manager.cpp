#include "register_manager.h"

RegisterManager::RegisterManager(QObject *parent) : QObject(parent)
{
    InitRegisterList(); // Инициализация
}

void RegisterManager::InitRegisterList()
{
    AddRegister(10005,REG_VISUALTYPE_NUMBER,"Регистр 1",REG_READWRITETYPE_READWRITE,0,REG_SYNCSTATUS_UNDEF);
    AddRegister(30200,REG_VISUALTYPE_LIGHTMODE,"Режим досветки грядки",REG_READWRITETYPE_READWRITE,0,REG_SYNCSTATUS_UNDEF);
    AddRegister(30204,REG_VISUALTYPE_TIMEx15,"Начало светового дня ",REG_READWRITETYPE_READWRITE,80,REG_SYNCSTATUS_UNDEF);
    AddRegister(30205,REG_VISUALTYPE_TIMEx15,"Продолжительность светового дня",REG_READWRITETYPE_READWRITE,48,REG_SYNCSTATUS_UNDEF);
    AddRegister(31204,REG_VISUALTYPE_TIMEx15,"Начало светового дня ",REG_READWRITETYPE_READWRITE,70,REG_SYNCSTATUS_SYNCRONIZED);
    AddRegister(31205,REG_VISUALTYPE_TIMEx15,"Продолжительность светового дня",REG_READWRITETYPE_READWRITE,20,REG_SYNCSTATUS_SYNCRONIZED);
}

void RegisterManager::AddRegister(quint16 RegisterCode,REG_VisualType VisualType, QString RegisterName, REG_ReadWriteType ReadWriteType, quint8 Value, REG_SyncStatus SyncStatus)
{

    RemoteRegister *newRegister = new RemoteRegister();
    newRegister->RegisterCode = RegisterCode;
    newRegister->VisualType = VisualType;
    newRegister->setValue(Value);
    newRegister->SyncStatus = SyncStatus;
    newRegister->ReadWriteType = ReadWriteType;
    newRegister->RegisterName = RegisterName;
    Registers.append(newRegister);
}

RemoteRegister* RegisterManager::GetRegisterByCode(quint16 RegisterCode)
{
    RemoteRegister *x;
    x=nullptr;
    for(quint8 i = 0; i < Registers.count();i++)
    {
        if(Registers[i]->RegisterCode == RegisterCode)
        {
            x = Registers.at(i);
        }
    }
    return x;
}

void RegisterManager::SetNewValue(quint16 RegisterCode, quint8 NewValue)
{
    for(quint8 i = 0; i < Registers.count();i++)
    {
        if(Registers[i]->RegisterCode == RegisterCode)
        {
            Registers[i]->setValue(NewValue);
            break;
        }
    }
}

void RegisterManager::tttt()
{
    for(quint8 i = 0; i < Registers.count();i++)
    {
        if(Registers[i]->RegisterCode == 30204)
        {
            Registers[i]->SyncStatus = REG_SYNCSTATUS_SYNCRONIZED;
            Registers[i]->setValue(32);
            break;
        }
    }
}
