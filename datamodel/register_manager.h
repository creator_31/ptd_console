#ifndef REGISTERMANAGER_H
#define REGISTERMANAGER_H

#include <QObject>
#include "remote_register.h"


class RegisterManager : public QObject
{
    Q_OBJECT
public:
    explicit RegisterManager(QObject *parent = nullptr);

    void InitRegisterList(void); // Наполнение списка регистров
    void AddRegister(quint16 RegisterCode,REG_VisualType VisualType, QString RegisterName, REG_ReadWriteType ReadWriteType, quint8 Value, REG_SyncStatus SyncStatus); // Добавление нового регистра
    RemoteRegister* GetRegisterByCode(quint16 RegisterCode);
    void SetNewValue(quint16 RegisterCode, quint8 NewValue); // Обновление значения регистра
    void tttt();
     QList<RemoteRegister*> Registers; // Основной массив регстров устройства
signals:

public slots:

private:

};

#endif // REGISTERMANAGER_H
