#ifndef REMOTEREGISTER_H
#define REMOTEREGISTER_H

#include <QObject>

enum REG_SyncStatus
{
    REG_SYNCSTATUS_UNDEF,
    REG_SYNCSTATUS_SYNCRONIZED,
    REG_SYNCSTATUS_NEED_READ,
    REG_SYNCSTATUS_NEED_WRITE,
    REG_SYNCSTATUS_TIMEOUT
};

enum REG_ReadWriteType
{
    REG_READWRITETYPE_READONLY,
    REG_READWRITETYPE_READWRITE
};

enum REG_VisualType // Определяет к какому типу относится регистр
{
    REG_VISUALTYPE_NUMBER,
    REG_VISUALTYPE_TIMEx15, // Время х15
    REG_VISUALTYPE_LIGHTMODE,
};


class RemoteRegister : public QObject
{
    Q_OBJECT

public:
    explicit RemoteRegister(QObject *parent = nullptr);
    quint16 RegisterCode; // Код регистра
    REG_VisualType VisualType; // Тип визуального представления регистра
    QString RegisterName; // Название регистра
    REG_SyncStatus SyncStatus; // Статус регистра
    REG_ReadWriteType ReadWriteType; // Доступ на запись
    quint8 Value();
    void setValue(quint8 newValue);

private:
    quint8 CurrentValue;   // Значенеи регистра
    quint8 NewValue; // Новое значение (при изменении)

signals:
    void RegisterUpdated(); // Сигнал обновления значения или статуса регистра

public slots:
};
//typedef RemoteRegisterLink as RemoteRegister

#endif // REMOTEREGISTER_H
