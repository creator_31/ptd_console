
#include "interface_controller.h"
#include "ui_interfacecontroller.h"
//#include <QtWidgets/QStackedWidget>

InterfaceController::InterfaceController(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InterfaceController)
{
    NavigationAllowFlag = false;
    CurrentInterfaceIndex = 0;
    InterfaceModeCurrent = INT_MODE_NAVIGATE;
    ui->setupUi(this);
}

InterfaceController::~InterfaceController()
{
    delete ui;
}

void InterfaceController::InintInterfaces()
{
    Interfaces.clear();

    // Заполнение списка
    quint16 ind_L1;
    quint16 ind_L2;

    ind_L1 = AddInterfaceWidget("Начальное окно","START",&INT_Start,65535);

    INT_Patch_A.SetPatch(0,"PATCH_A");
    connect(&INT_Patch_A,SIGNAL(NavigateToInterface(QString)),this,SLOT(GoToInterfacebyCode(QString)));
    //INT_Patch_A.SetRegisterManager(&REG_M);
    ind_L1 = AddInterfaceWidget("Грядка A","PATCH_A",&INT_Patch_A,65535);

    INT_PatchA_Lighting.SetPatchNumber(0);
    INT_PatchA_Lighting.SetRegisterManager(&REG_M);
    ind_L2 = AddInterfaceWidget("Освещение грядки A","PATCH_A-LIGHTING",&INT_PatchA_Lighting,ind_L1);


    INT_Patch_B.SetPatch(1,"PATCH_B");
    connect(&INT_Patch_B,SIGNAL(NavigateToInterface(QString)),this,SLOT(GoToInterfacebyCode(QString)));
    //INT_Patch_B.SetRegisterManager(&REG_M);
    ind_L1 = AddInterfaceWidget("Грядка B","PATCH_B",&INT_Patch_B,65535);

    INT_PatchB_Lighting.SetPatchNumber(1);
    INT_PatchB_Lighting.SetRegisterManager(&REG_M);
    ind_L2 = AddInterfaceWidget("Освещение грядки B","PATCH_B-LIGHTING",&INT_PatchB_Lighting,ind_L1);


    // Загрузка и настройка визульной части
    for(quint16 i =0; i< Interfaces.count();i++)
    {
        ui->WidgetStack->addWidget(Interfaces[i].InterfaceObjectLink);
    }
    ui->WidgetStack->setCurrentIndex(0);

    //На время отладки
    connect(this,SIGNAL(sig_NAV_Back()),this,SLOT(NAV_Back()));
    connect(this,SIGNAL(sig_NAV_CCW()),this,SLOT(NAV_CCW()));
    connect(this,SIGNAL(sig_NAV_CW()),this,SLOT(NAV_CW()));
    connect(this,SIGNAL(sig_NAV_OK()),this,SLOT(NAV_OK()));

}

void InterfaceController::NAV_Back()
{
    if(InterfaceModeCurrent == INT_MODE_NAVIGATE) // Если в режиме навигации
    {
        ui->label_2->setText("BACK");

        quint16 ind_next;

        ind_next = Interfaces[CurrentInterfaceIndex].ParentIndex;

        if(ind_next < 65535)
        {
            CurrentInterfaceIndex = ind_next;
            ui->WidgetStack->setCurrentIndex(ind_next);
        }
        else // Если кнопка назад нажаьа в верхнем уровне меню - то переходим на стартовый экран
        {
            CurrentInterfaceIndex = 0;
            ui->WidgetStack->setCurrentIndex(0);
        }
    }
}

void InterfaceController::NAV_CCW()
{
    if(InterfaceModeCurrent == INT_MODE_NAVIGATE) // Если в режиме навигации
    {
        ui->label_2->setText("CCW");
        quint16 ind_next;
        ind_next = CurrentInterfaceIndex;
        if(ind_next > 0) // Если сейчас активен не первый интерфейс
        {
            ind_next--; //Двигаем указатель на пред. позицию
        }
        else
        {
            ind_next = Interfaces.count()-1; // двигаем на последний элемент списка
        }
        while(ind_next != CurrentInterfaceIndex)
        {
            if(Interfaces[ind_next].ParentIndex == Interfaces[CurrentInterfaceIndex].ParentIndex) // если найден следующий интрефейс с тем-же родителем
            {
                break;
            }
            if(ind_next >0) // Если это не первый интерфейс
            {
                ind_next--; //Двигаем указатель на след. позицию
            }
            else
            {
                ind_next = Interfaces.count()-1; // двигаем на последний элемент списка
            }
        }
        CurrentInterfaceIndex = ind_next;
        ui->WidgetStack->setCurrentIndex(ind_next);
    }
}

void InterfaceController::NAV_CW()
{
    if(InterfaceModeCurrent == INT_MODE_NAVIGATE) // Если в режиме навигации
    {
        ui->label_2->setText("CW");
        quint16 ind_next;
        ind_next = CurrentInterfaceIndex;
        if(ind_next < Interfaces.count()-1) // Если сейчас активен не последний интерфейс
        {
            ind_next++; //Двигаем указатель на след. позицию
        }
        else
        {
            ind_next = 0; // двигаем на первый элемент списка
        }
        while(ind_next != CurrentInterfaceIndex)
        {
            if(Interfaces[ind_next].ParentIndex == Interfaces[CurrentInterfaceIndex].ParentIndex) // если найден следующий интрефейс с тем-же родителем
            {
                break;
            }
            if(ind_next +1 < Interfaces.count()) // Если сейчас активен не последний интерфейс
            {
                ind_next++; //Двигаем указатель на след. позицию
            }
            else
            {
                ind_next = 0; // двигаем на первый элемент списка
            }
        }
        CurrentInterfaceIndex = ind_next;
        ui->WidgetStack->setCurrentIndex(ind_next);
    }
}

void InterfaceController::NAV_OK()
{

}

void InterfaceController::GoToInterfacebyCode(QString InterfaceCode)
{
    for(quint16 i =0; i< Interfaces.count();i++)
    {
        if(Interfaces[i].InterfaceCode == InterfaceCode)
        {
            CurrentInterfaceIndex = i;
            ui->WidgetStack->setCurrentIndex(i);
            break;
        }
    }
}

quint16 InterfaceController::AddInterfaceWidget(QString InterfaceName,QString InterfaceCode, QWidget *InterfaceObject,quint16 ParentIndex)
{
    InterfaceItem NewInterface;
    NewInterface.InterfaceName = InterfaceName;
    NewInterface.InterfaceObjectLink = InterfaceObject;
    NewInterface.InterfaceCode = InterfaceCode;
    NewInterface.ParentIndex = ParentIndex;
    Interfaces.append(NewInterface);

    quint16 r = Interfaces.count()-1;
    return r;
}



void InterfaceController::on_BUT_CCW_clicked()
{
    emit this->NAV_CCW();
}

void InterfaceController::on_BUT_Back_clicked()
{
    emit this->NAV_Back();
}

void InterfaceController::on_BUT_CW_clicked()
{
    emit this->NAV_CW();
}

void InterfaceController::on_BUT_OK_clicked()
{
    emit this->NAV_OK();
}
