#ifndef INTERFACE_CONTROLLER_H
#define INTERFACE_CONTROLLER_H

#include <QWidget>
#include "datamodel/register_manager.h"
#include "interface_wid/iw_start_form.h"
#include "interface_wid/iw_patch_main.h"
#include "interface_wid/iw_patchlighting.h"

namespace Ui {
class InterfaceController;
}



typedef struct
{
    quint16 ParentIndex; // Индекс родительского интерфейса
    QString InterfaceName; // Название интерфейса
    QString InterfaceCode; // Для поиска
    QWidget* InterfaceObjectLink; // Указатель на объекта InterfaceWidget
} InterfaceItem;

enum InterfaceMode
{
    INT_MODE_NAVIGATE,
    INT_MODE_EDIT_PARAM
};


class InterfaceController : public QWidget
{
    Q_OBJECT

public:
    explicit InterfaceController(QWidget *parent = nullptr);
    ~InterfaceController();
    void InintInterfaces(); // Инициализация интерфейсов

signals:
    void sig_NAV_Back();
    void sig_NAV_CCW(); // Энкодер против часовой стрелки (минус)
    void sig_NAV_CW(); // Энкодер по часовой стрелки (плюс)
    void sig_NAV_OK(); // Энкодер нажатие кнопки


public slots:
    void NAV_Back(); // Нажатие кнопки назад/отмена
    void NAV_CCW(); // Энкодер против часовой стрелки (минус)
    void NAV_CW(); // Энкодер по часовой стрелки (плюс)
    void NAV_OK(); // Энкодер нажатие кнопки
    void GoToInterfacebyCode(QString InterfaceCode); // Навигация к интерфейсу по коду
private slots:
    void on_BUT_CCW_clicked();

    void on_BUT_Back_clicked();

    void on_BUT_CW_clicked();

    void on_BUT_OK_clicked();

private:
    Ui::InterfaceController *ui;

    RegisterManager REG_M; // Менеджер и хранилище данных регистров

    QList<InterfaceItem> Interfaces; // Основной массив интерфейсов
    quint16 AddInterfaceWidget(QString InterfaceName,QString InterfaceCode, QWidget* InterfaceObject,quint16 ParentIndex); // Добавление нового интерфейса в навигацию

    quint16 CurrentInterfaceIndex; // Индекс текущего интерфейса
    bool NavigationAllowFlag; // Флаг разрешения навигации

    InterfaceMode InterfaceModeCurrent; // Текущий режим работы интерфейса
    // --- Интерфейсы
    IW_start_form INT_Start; // Стартовая форма
    IW_patch_main INT_Patch_A; // Основной интерфейс грядки А
    IW_PatchLighting INT_PatchA_Lighting; // Управление светом грядки А
    IW_patch_main INT_Patch_B; // Основной интерфейс грядки B
    IW_PatchLighting INT_PatchB_Lighting; // Управление светом грядки А
};

#endif // INTERFACE_CONTROLLER_H
